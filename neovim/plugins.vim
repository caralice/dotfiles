if &compatible
  set nocompatible
endif

set rtp+=/usr/share/vim/vimfiles
filetype off

call dein#begin(expand('~/.local/share/dein'))

" Eye candy
call dein#add('gruvbox-community/gruvbox')
call dein#add('ryanoasis/vim-devicons')
call dein#add('vim-airline/vim-airline')
call dein#add('vim-airline/vim-airline-themes')
call dein#add('wsdjeg/dein-ui.vim')
call dein#add('chrisbra/Colorizer')

" Analysis
"call dein#add('jackguo380/vim-lsp-cxx-highlight')
call dein#add('dense-analysis/ale')
call dein#add('neoclide/coc.nvim', {'merged': 0, 'rev': 'release'})
call dein#add('vim-vdebug/vdebug')

" Languages
call dein#add('dmix/elvish.vim', {'on_ft': ['elvish']})
call dein#add('hylang/vim-hy')
call dein#add('killphi/vim-ebnf', {'on_ft': ['ebnf']})
call dein#add('nvim-treesitter/nvim-treesitter')
call dein#add('ron-rs/ron.vim')
call dein#add('sheerun/vim-polyglot')
call dein#add('lervag/vimtex')
call dein#add('thyrgle/vim-dyon')
call dein#add('tjdevries/coc-zsh')
call dein#add('rubixninja314/vim-mcfunction', {'rev': 'early_access'})

" Misc
call dein#add('glacambre/firenvim')
call dein#add('inkarkat/vim-SyntaxRange')
call dein#add('mbbill/undotree')
call dein#add('tpope/vim-sensible')
call dein#add('tpope/vim-abolish')
call dein#add('vim-scripts/loremipsum')
call dein#add('iamcco/markdown-preview.nvim', {
      \ 'on_ft': ['markdown', 'pandoc.markdown', 'rmd'],
      \ 'build': 'sh -c "cd app && yarn install"'
      \ })

call dein#end()

"if dein#check_install()
"  call dein#install()
"endif

filetype plugin on

let g:coc_global_extensions = [
  \ 'coc-clangd',
  \ 'coc-cmake',
  \ 'coc-css',
  \ 'coc-deno',
  \ 'coc-discord-rpc',
  \ 'coc-elixir',
  \ 'coc-emmet',
  \ 'coc-explorer',
  \ 'coc-fish',
  \ 'coc-flutter',
  \ 'coc-git',
  \ 'coc-highlight',
  \ 'coc-java',
  \ 'coc-json',
  \ 'coc-markdownlint',
  \ 'coc-pairs',
  \ 'coc-powershell',
  \ 'coc-prettier',
  \ 'coc-pyright',
  \ 'coc-rust-analyzer',
  \ 'coc-sh',
  \ 'coc-snippets',
  \ 'coc-svg',
  \ 'coc-toml',
  \ 'coc-translator',
  \ 'coc-vimlsp',
  \ 'coc-vimtex',
\ ]
