augroup vimrc
  autocmd! BufWritePost $MYVIMRC source % | echom "Reloaded " . $MYVIMRC | redraw
  autocmd! BufWritePost $MYGVIMRC if has('gui_running') | so % | echom "Reloaded " . $MYGVIMRC | endif | redraw
augroup end

augroup every
  autocmd!
  autocmd InsertEnter * set norelativenumber
  autocmd InsertLeave * set relativenumber
augroup end

"autocmd FileType markdown :call SyntaxRange#Include('+++', '+++', 'toml')

autocmd FileType fish compiler fish
autocmd FileType fish setlocal textwidth=79
autocmd FileType fish setlocal foldmethod=expr

augroup syntax_aliases
  autocmd BufNewFile,BufRead *.do setlocal filetype=sh
augroup end

"autocmd VimEnter * ColorHighlight
