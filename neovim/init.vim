runtime settings.vim
runtime functions.vim
runtime plugins.vim
runtime aucmd.vim
runtime keys.vim
" vim: ts=4 sw=4
