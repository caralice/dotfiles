nnoremap <Space> za
nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-l> <c-w>l
nnoremap <leader>e :exe getline(line('.'))<cr>
nnoremap <leader>ld :LivedownToggle<CR>
nmap <leader>rn <Plug>(coc-rename)

nnoremap S "_D
nnoremap s "_d
nnoremap ss "_dd

nnoremap j gj
nnoremap gj j
nnoremap k gk
nnoremap gk k
nmap <Up> k
nmap <Down> j
