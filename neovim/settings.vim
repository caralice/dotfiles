set clipboard=unnamedplus
set conceallevel=2
set cursorline
set expandtab
set exrc
set foldmethod=syntax
set linebreak
set list
set listchars=eol:↵,trail:·,tab:<-,nbsp:·,space:·
set mouse=a
set noshowmode
set number
set secure
set shell=zsh
set shiftwidth=4
set smartcase
set tabstop=4
set undofile
set termguicolors
set guifont=JetBrainsMono\ Nerd\ Font\ Mono:h15

"let g:gruvbox_improved_strings=1
let g:gruvbox_improved_warnings=1
let g:gruvbox_italic=1
let g:gruvbox_italicize_strings=1

let g:Powerline_symbols='unicode'
let g:airline#extensions#keymap#enabled = 0
let g:airline_left_alt_sep = "\ue0b5"
let g:airline_left_sep = "\ue0b4"
let g:airline_powerline_fonts = 1
let g:airline_right_alt_sep = "\ue0b7"
let g:airline_right_sep = "\ue0b6"
let g:airline_skip_empty_sections = 1
let g:airline_theme='base16_gruvbox_dark_medium'

let g:ale_disable_lsp = 1
let g:ale_fix_on_save = 1
let g:ale_sign_error = "\uf46e"
let g:ale_sign_warning = "\uf071"

let g:ale_fixers = {
      \ '*' : ['remove_trailing_lines', 'trim_whitespace'],
      \ 'cpp': ['clang-format'],
      \ 'c': ['clang-format'],
      \ 'css': ['prettier'],
      \ 'html': ['prettier'],
      \ 'javascript': ['prettier'],
      \ 'json': ['jq', 'fixjson', 'prettier'],
      \ 'markdown': ['prettier'],
      \ 'perl': ['perltidy'],
      \ 'python': ['black', 'isort'],
      \ 'rust': ['rustfmt'],
      \ 'yaml': ['prettier'],
      \ }
let g:ale_linters = {
      \ 'rust': ['cargo', 'analyzer'],
      \ 'perl': ['perl', 'perlcritic'],
      \ 'python': ['mypy', 'flake8'],
      \ 'cpp': ['clangd', 'clang-tidy'],
      \ 'dart': ['dartfmt'],
      \ 'markdown': ['mdl'],
      \ }

let g:ale_nasm_nasm_options = '-felf64'

let g:asmsyntax = 'nasm'

let g:hy_enable_conceal = 1
let g:hy_conceal_fancy = 1

let g:vim_markdown_fenced_languages = ['rs=rust', 'py=python', 'x86asm=nasm']
let g:vim_markdown_no_extensions_in_markdown = 1

let g:mkdp_auto_start = 1

let g:colorizer_auto_filetype='css,html,mplstyle'

let g:neovide_cursor_vfx_mode = "wireframe"

let g:polyglot_disabled = ['indent']

colorscheme gruvbox

if exists('g:started_by_firenvim')
    set laststatus=0
"    set background=light
else
end
