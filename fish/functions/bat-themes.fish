set _bat_show_cmd "bat ~/src/bat/assets/theme_preview.rs --theme {} --color=always --decorations=never"
function bat-themes --wraps=$_bat_show_cmd
  set -la _bat_show_cmd $argv
  bat --list-themes 2>/dev/null \
  | fzf --preview="$_bat_show_cmd";
end
