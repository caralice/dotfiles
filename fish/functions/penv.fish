function penv
    set file /proc/$argv[1]/environ
    bat $file \
    | string split0 \
    | bat -ldotenv --file-name $file
end
