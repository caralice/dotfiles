# Defined in - @ line 1
function compose --wraps='rg $argv[1] /usr/share/X11/locale/en_US.UTF-8/Compose' --description 'alias compose rg $argv[1] /usr/share/X11/locale/en_US.UTF-8/Compose'
  rg $argv[1] /usr/share/X11/locale/en_US.UTF-8/Compose $argv[2..-1];
end
