# Defined in - @ line 1
function ls --wraps='exa --git -hgb --icons' --description 'alias ls exa --git -hgb --icons'
  exa --git -hgb --icons $argv;
end
