function ct
    set -l templates $HOME/templates/
    set -l template_name $argv[1]
    set -l template "$templates/$template_name"
    set -l target $argv[2]
    cp -TLnr "$template" "$target"
    if not test -d "$target"
        return
    end
    cd $target
    if test -f .create
        source .create $argv
        rm .create
    end
end
