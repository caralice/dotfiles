--italic-text=always

# [[ Syntax mappings
--map-syntax "*.xaml:XML"
--map-syntax "pack.mcmeta:JSON"
--map-syntax ".flake8:INI"
--map-syntax ".clang-format:YAML"
--map-syntax "requirements*.txt:requirements.txt"
--map-syntax "bat:Bourne Again Shell (bash)"
--map-syntax "**/.cargo/config:TOML"
# [[ AVR
--map-syntax "*.aws:XML"
--map-syntax "*.aps:XML"
--map-syntax "labels.tmp:XML"
--map-syntax "*.simu:XML" # SimulIDE
# ]]
--map-syntax ".exrc:VimL"
--map-syntax "*.do:Bourne Again Shell (bash)"
# ]]
# vim: foldmethod=marker foldmarker=[[,]]
